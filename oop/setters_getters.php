<?php


class Cars {

    private $door_count = 4;
   
    function get_values(){
       return $this->door_count;
    }

    function set_values($value){
        $this->door_count = $value;
    }

}

$bmw = new Cars();
$bmw->set_values(20);
echo $bmw->get_values();

